let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let studSchema = new Schema({
    name: {
        type: String
    }
})

let Student = mongoose.model('Student',studSchema);

module.exports = Student;
