let express = require('express');
let mongoose = require('mongoose');
let Student = require('./models/student')

let app = express();


//connect to db
let dburl = "mongodb://localhost:27017/student";
mongoose.connect(dburl,{useNewUrlParser: true, useUnifiedTopology: true,useFindAndModify: false})
    .then((result) => app.listen(8080))
    .catch((err) => console.log(err));

//mongoose and mongo routes


app.get('/',(req,res) => {
    res.redirect('/all-stud');
})

//add student 
app.get('/add-stud',(req,res) => {

    let stud = new Student({
        name: 'Nodejs1'
    });
    stud.save()
        .then((result) => {
            res.send(result)
        })
        .catch((err) => {
            console.log(err);
        });
})


//list student
app.get('/all-stud',(req,res) => {
    Student.find()
        .then((result) => {
            res.send(result);
        })
        .catch((err) =>{
            console.log(err);
        });

})

//update
app.get('/update-stud',(req,res) => {
    Student.findByIdAndUpdate('611e3ff5e8e0e62dec7844ab',{name:'nodeupdated'})
        .then((result) => {
            res.send("Updation Done");
        })
        .catch((err) => {
            console.log(err);
        });

})


//delete
app.get('/delete-stud',(req,res) => {
    Student.findByIdAndDelete('611e49c984963e424890ff67')
        .then((result) => {
            res.send("Deleted");

        })
        .catch((err) => {
            console.log(err);

        });


})


