let express = require('express');
let app = express();

app.use(express.json());

let mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/student',{

},
err =>{
    if(err)
    {
        console.log(err);
    }
    else{
        console.log("conn done");
    }
})

let studentdb =  new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String
    }
})

mongoose.model("stud",studentdb);
//READ Request handler
app.get('/',(req,res) => {

    res.send(
        `<h2>Welcome to students database!</h2>
        <h3> Click here to get the <b><a href="/student/list">Database</a></b></h3>`
    )
});

app.listen(8080,() => {
    console.log("server started at port 8080");
});